import java.util.ArrayList;

/**
 * @author Marc Nuño Espinosa
 *
 */
public class juego {
	/**
	 * Booleano que indica si el personaje está mirando hacia arriba o no.
	 */
	static boolean mirarW = false;
	/**
	 * Booleano que indica si el personaje está mirando hacia la izquierda o no.
	 */
	static boolean mirarA = false;
	/**
	 * Booleano que indica si el personaje está mirando hacia la derecha o no.
	 */
	static boolean mirarD = false;
	/**
	 * Booleano que indica si el personaje está mirando hacia abajo o no.
	 */
	static boolean mirarS = false;
	/**
	 * Matriz creada para poder cambiar de una matriz a otra sin intercambiar sus
	 * valores.
	 */
	static int[][] intermedia;
	/**
	 * Númeron entero y positivo que indica la fila de la matriz en la que se
	 * encuentra nuestro personaje.
	 */
	static int x;
	/**
	 * Númeron entero y positivo que indica la columna de la matriz en la que se
	 * encuentra nuestro personaje.
	 */
	static int y;
	/**
	 * Matriz vinculada a la imagen de fondo "Patio.png"
	 */
	static int[][] patio = { { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
			{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
			{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
			{ 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1 },
			{ 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1 },
			{ 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1 },
			{ 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1 },
			{ 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1 },
			{ 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1 },
			{ 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1 },
			{ 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1 },
			{ 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1 },
			{ 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1 },
			{ 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1 },
			{ 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1 },
			{ 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1 } };

	/**
	 * Matriz vinculada a la imagen de fondo "Orfanato.png"
	 */
	static int[][] orfanato = { { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
			{ 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1 },
			{ 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1 },
			{ 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1 },
			{ 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1 },
			{ 1, 1, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 1 },
			{ 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1 },
			{ 1, 1, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 1 },
			{ 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1 },
			{ 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1 },
			{ 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1 },
			{ 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1 },
			{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1 } };

	/**
	 * Matriz vinculada a la imagen de fondo "Cueva.png" y "CuevaConCuerda.png"
	 */
	static int[][] cueva = { { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1 },
			{ 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1 },
			{ 1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1 },
			{ 1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1 },
			{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 } };

	/**
	 * Matriz vinculada a la imagen de fondo "Bosque.png"
	 */
	static int[][] bosque = {
			{ 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
			{ 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1 },
			{ 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1 },
			{ 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1 },
			{ 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1 },
			{ 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
			{ 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1 },
			{ 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1 },
			{ 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1 },
			{ 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1 },
			{ 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
			{ 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
			{ 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
			{ 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
			{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 } };

	/**
	 * Matriz vinculada a la imagen de fondo "Tienda.png"
	 */
	static int[][] tienda = { { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }, { 1, 0, 0, 1, 0, 0, 0, 0, 1, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 0, 1, 1 }, { 1, 0, 0, 0, 1, 1, 1, 0, 1, 1 }, { 1, 0, 0, 0, 1, 1, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 0, 1, 0, 0, 0, 1 }, { 1, 0, 0, 0, 0, 0, 0, 0, 0, 1 }, { 1, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 1 }, { 1, 1, 1, 1, 0, 0, 1, 1, 1, 1 } };

	/**
	 * Número entero y positivo utilizado para vincular las matrices a un
	 * identificador.
	 */
	static int pantalla = 0; // (patio=1, orfanato=2, cueva=3, bosque=4, tienda=5;
	static Board b = new Board();
	static Window f = new Window(b);
	static ArrayList<String> inventario = new ArrayList<String>();
	// ArrayList creada para simular un inventario, en un futuro añadiré más objetos
	// al juego.
	static String[] imagenes = { "", "", "EdrickWalkingA.png", "EdrickWalkingW.png", "EdrickWalkingS.png",
			"EdrickWalkingD.png" };
	// Array de strings que contiene las imagenes utilizadas en las matrices. (el 0
	// y 1 están reservados para hacer algo traspasable o no).
	static boolean jugando = false;
	// Booleano para determinar si estamos o no jugando. Si mueres se coloca en
	// false y tienes que volver a pasar por el menú (aun no está implementado).

	public static void main(String[] args) throws InterruptedException {
		b.setText(args);
		ini();
		while (!morir()) {
			Thread.sleep(100);
			b.draw(intermedia);
			// System.out.println("Pantalla: " + pantalla + " Posición:" + x + " " + y);
			move();
			turn();
			use();
			change();

		}
	}

	/**
	 * Función que imprime de fondo la imagen has muerto si cumples alguna de las
	 * condiciones de muerte.
	 */
	private static boolean morir() {
		if (pantalla == 3) {
			if ((x == 10 || x == 11) && y == 1) {
				cueva[x][y] = 0;
				b.setImgbackground("dead.png");
				jugando = false;
				return true;
			}
		}
		return false;
	}

	/**
	 * Funcion que permite ir cambiando de pantalla según tu posición y pantalla
	 * actual
	 */
	private static void change() {
		if (pantalla == 1) {
			if (x == 11 && y == 7) {
				pantalla = 2;
				intermedia = orfanato;
				patio[11][7] = 0;
				x = 14;
				y = 10;
				intermedia[x][y] = 3;
				b.setImgbackground("Orfanato.png");
				b.draw(orfanato);
			}
			if (x == 3 && y == 4) {
				pantalla = 3;
				intermedia = cueva;
				patio[3][4] = 0;
				x = 3;
				y = 2;
				;
				if (mirarW == true)
					intermedia[x][y] = 3;
				if (mirarA == true)
					intermedia[x][y] = 2;
				if (mirarD == true)
					intermedia[x][y] = 5;
				b.draw(cueva);
				if (!inventario.contains("cuerda")) {
					b.setImgbackground("CuevaConCuerda.png");
				} else {
					b.setImgbackground("Cueva.png");
				}
			}
			if (x == 15 && y > 2 && y < 16) {
				pantalla = 4;
				intermedia = bosque;
				patio[x][y] = 0;
				x = 1;
				intermedia[x][y] = 4;
				b.setImgbackground("Bosque.png");
				b.draw(bosque);
			}
		}
		if (pantalla == 2) {
			if ((x == 15 && y == 9) || (x == 15 && y == 10)) {
				pantalla = 1;
				intermedia = patio;
				orfanato[15][9] = 0;
				orfanato[15][10] = 0;
				x = 12;
				y = 7;
				intermedia[x][y] = 4;
				b.setImgbackground("Patio.png");
				b.draw(patio);

			}
			if (x == 1 && y == 14 && mirarW == true) {
				do {
					if (f.getPressedKeys().contains('f')) {
						orfanato[x][y] = 0;
						b.setImgbackground("Carlonda.png");
					}
				} while (f.getPressedKeys().contains('f'));
				orfanato[x][y] = 3;
				b.setImgbackground("Orfanato.png");
				b.draw(orfanato);
			}
			if (x == 14 && y == 13) {
				if (inventario.contains("cuerda")) {
					pantalla = 3;
					intermedia = cueva;
					orfanato[14][13] = 0;
					x = 8;
					y = 14;
					if (mirarS == true)
						intermedia[x][y] = 4;
					if (mirarA == true)
						intermedia[x][y] = 2;
					if (mirarD == true)
						intermedia[x][y] = 5;
					b.setImgbackground("Cueva.png");
					b.draw(cueva);
				} else {
					pantalla = 3;
					intermedia = cueva;
					orfanato[14][13] = 0;
					x = 8;
					y = 14;
					if (mirarS == true)
						intermedia[x][y] = 4;
					if (mirarA == true)
						intermedia[x][y] = 2;
					if (mirarD == true)
						intermedia[x][y] = 5;
					b.setImgbackground("CuevaConCuerda.png");
					b.draw(cueva);
				}
			}
		}
		if (pantalla == 3) {
			if ((x == 3 && y == 2 && mirarD == true) || (x == 4 && y == 3 && mirarW == true)
					|| (x == 3 && y == 4 && mirarA == true) || (x == 2 && y == 3 && mirarS == true)
					|| (x == 3 && y == 3)) {
				if (f.getPressedKeys().contains('f')) {
					inventario.add("cuerda");
					b.setImgbackground("Cueva.png");

				}
			}
			if (x == 8 && y == 14 && inventario.contains("cuerda") && f.getPressedKeys().contains('f')) {
				pantalla = 2;
				intermedia = orfanato;
				cueva[8][14] = 0;
				if (mirarA == true) {
					intermedia[14][12] = 2;
					x = 14;
					y = 12;
				}
				if (mirarW == true || mirarS == true) {
					intermedia[13][13] = 3;
					x = 13;
					y = 13;
				}
				if (mirarD == true) {
					intermedia[14][14] = 5;
					x = 14;
					y = 14;
				}
				b.setImgbackground("Orfanato.png");
				b.draw(orfanato);
			}
		}
		if (pantalla == 4) {
			if (x == 6 && y == 21) {
				pantalla = 5;
				intermedia = tienda;
				bosque[6][21] = 0;
				x = 8;
				y = 4;
				intermedia[x][y] = 3;
				b.setImgbackground("Tienda.png");
				b.draw(tienda);
			}
			if (x == 0 && y > 2 && y < 16) {
				pantalla = 1;
				intermedia = patio;
				bosque[x][y] = 0;
				x = 14;
				intermedia[x][y] = 3;
				b.setImgbackground("Patio.png");
				b.draw(patio);
			}
		}
		if (pantalla == 5) {
			if ((x == 9 && y == 4) || (x == 9 && y == 5)) {
				if (f.getPressedKeys().contains('s')) {
					pantalla = 4;
					intermedia = bosque;
					tienda[x][y] = 0;
					x = 7;
					y = 21;
					intermedia[x][y] = 4;
					b.setImgbackground("Bosque.png");
					b.draw(bosque);
				}
			}
			if (x == 1 && y == 6 && mirarW == true) {
				do {
					if (f.getPressedKeys().contains('f')) {
						tienda[x][y] = 0;
						b.setImgbackground("Retrato.png");
					}
				} while (f.getPressedKeys().contains('f'));
				tienda[x][y] = 3;
				b.setImgbackground("Tienda.png");
				b.draw(tienda);
			}
		}
	}

	private static void use() {
		if (f.getPressedKeys().contains('f')) {
			System.out.println("Usaste esto");
			// Permite recoger objetos e interactuar con el mapa.
		}
	}

	private static void ini() {
		b.setActimgbackground(true);
		b.setImgbackground("Patio.png");
		b.setActborder(false);
		b.setSprites(imagenes);
		f.setActLabels(false);
		f.setTitle("SUPERANDO DAMVI");
		jugando = true;
		intermedia = patio;
		b.draw(intermedia);
		x = 14;
		y = 13;
		intermedia[x][y] = 4;
		pantalla = 1;

	}

	/**
	 * Función de movimiento, se juega con w|a|s|d
	 */
	public static void move() {
		if (f.getPressedKeys().contains('a') && intermedia[x][y - 1] == 0) {
			intermedia[x][y] = 0;
			intermedia[x][y - 1] = 2;
			mirarW = false;
			mirarA = true;
			mirarS = false;
			mirarD = false;
			y--;
		}
		if (f.getPressedKeys().contains('w') && intermedia[x - 1][y] == 0) {
			intermedia[x][y] = 0;
			intermedia[x - 1][y] = 3;
			mirarW = true;
			mirarA = false;
			mirarS = false;
			mirarD = false;
			x--;
		}
		if (f.getPressedKeys().contains('s') && intermedia[x + 1][y] == 0) {
			intermedia[x][y] = 0;
			intermedia[x + 1][y] = 4;
			mirarW = false;
			mirarA = false;
			mirarS = true;
			mirarD = false;
			x++;
		}
		if (f.getPressedKeys().contains('d') && intermedia[x][y + 1] == 0) {
			intermedia[x][y] = 0;
			intermedia[x][y + 1] = 5;
			mirarW = false;
			mirarA = false;
			mirarS = false;
			mirarD = true;
			y++;

		}

	}

	/**
	 * Función de girarse, se activa con Shift + W|A|S|D (o al pulsar éstas teclas
	 * con el Bloq Mayús activado)
	 */
	public static void turn() {
		if (f.getPressedKeys().contains('A')) {
			intermedia[x][y] = 0;
			mirarW = false;
			mirarA = true;
			mirarS = false;
			mirarD = false;
			intermedia[x][y] = 2;
		}
		if (f.getPressedKeys().contains('W')) {
			intermedia[x][y] = 0;
			mirarW = true;
			mirarA = false;
			mirarS = false;
			mirarD = false;
			intermedia[x][y] = 3;
		}
		if (f.getPressedKeys().contains('S')) {
			intermedia[x][y] = 0;
			mirarW = false;
			mirarA = false;
			mirarS = true;
			mirarD = false;
			intermedia[x][y] = 4;
		}
		if (f.getPressedKeys().contains('D')) {
			intermedia[x][y] = 0;
			mirarW = false;
			mirarA = false;
			mirarS = false;
			mirarD = true;
			intermedia[x][y] = 5;
		}
	}
}
